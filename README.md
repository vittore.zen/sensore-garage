# Sensore garage

Un sensore di presenza integrato in home assistant che utilizza la tecnologia LoRaWAN e la rete open TTN. L'obiettivo è monitorare la presenza delle auto nel garage che si trova ad una distanza non copribile con reti wireless o reti ethernet.

## Lista della spesa
Per monitorare 2 spazi (garage e posto auto).

- nr. 2 sensori ad ultrasuoni SR-04
- nr. 1 scheda arduino [MKR WAN 1310](https://docs.arduino.cc/hardware/mkr-wan-1310)
- nr. 1 antenna kit [Mikrotik wAP LR8](https://mikrotik.com/product/wap_lr8_kit) - Non necessaria se la vostra area è già coperta da TTN (vedi [https://ttnmapper.org/heatmap/](https://ttnmapper.org/heatmap/))

## Link per approfondire
- [LoRaWAN, cos’è, a cosa serve, perché conviene utilizzarla](https://www.internet4things.it/industry-4-0/lorawan-cose-a-cosa-serve-perche-conviene-utilizzarla/)
- [Capitolo 6](https://github.com/piercalderan/LoRa_LoraWAN/blob/main/LORA_CAP6_AGGIORNATO.pdf) del libro  "Capire e usare LoRa e LoraWAN"  di Pier Calderan
- [The Things Network: una rete internet delle cose in ogni metropoli](https://www.agendadigitale.eu/smart-city/the-things-network-una-rete-internet-delle-cose-in-ogni-citta-del-mondo/)
- [Connecting MKR WAN 1310 to The Things Network (TTN)](https://docs.arduino.cc/tutorials/mkr-wan-1310/the-things-network)

## Arduino

Connettere i due sensori SR-4 con:

- arduino pin 6 - sensore 1 pin echo
- arduino pin 7 - sensore 1 pin trig
- arduino pin 8 - sensore 2 pin echo
- arduino pin 9 - sensore 2 pin trig
- arduino pin GND - sensore 1 e 2 pin GND
- arduino pin Vcc - sensore 1 e 2 pin Vcc

![SR 04 pinout](https://repository-images.githubusercontent.com/113425541/b2fa0c80-7d53-11e9-8f6a-07114b1895cf)


## Codice
[Codice per arduino MKR 1310](codes/lora.ino)

## Passi
### 1. Configurazione gateway Mikrotik

- Collegare l'antenna mikrotik alla rete ethernet
- Dal sito Mikrotik sezione download scaricare l'applicazione Winbox
- Avviare l'applicazione
- Nel tab Neigbour compare l'antenna con il suo mac address ed ip
- Fare click sopra il mac address e poi click su connect (Username admin password vuota)
- Click su System > Reset configuration
- Spuntare le voci: "No Default Configuration" e "Do not backup"
- Confermare. Il router si riavvia per il reset di fabbrica.
- Dopo un po' ritorna raggiungibile da Winbox. Connettersi.
- Dalla interfaccia di amministrazione scegliere "New terminal"
- Copiare ed incollare il codice [disponibile qui](codes/lora-gateway.rsc)
- Riavviare il gateway con l'opzione System > Reboot
- Connettersi nuovamente utilizzando winbox
- Andare nel menù Lora > Device > gateway-0
- Nel tab General prendere nota dell'indentificativo presente nel campo "Gateway ID"


### 2. Registrazione del gateway su TTN
- Creare un account gratuito su TTN 
- Dalla [console](https://eu1.cloud.thethings.network/console/) scegliere l'icona gateway
- Scegliere l'opzione Register gateway
- Nel campo "Gateway EUI" indicare quando precedentemente riportato nel gateway mikrotik alla voce "Gateway ID"
- In "Frequency plan" scegliere "Europe 863-870 MHz (SF9 for RX2 - recommended)"
- Compilate i campi restanti come desiderate. E' importante per la community TTN popolare il campo della posizione geografica.

Se tutto è corretto in "Live" si vedono i dati che fluiscono dal gateway.

### 3. Configurazione del dispositivo IoT
- Avviare l'applicazione arduino e recuperare l'ID che viene visualizzato
- Andare in [TTN application](https://eu1.cloud.thethings.network/console/applications) e creare una nuova applicazione (check-garage)
- Registrare un nuovo end device indicando:
  - DevEUI indicare l'id ottenuto dall'applicazione arduino
  - JoinEUI: 0000000000000000
- Aggiungere una API key (menù API key > add API key)
- Riportare l'API key nel codice arduino alla riga 15 variabile appKey

In live data dell'applicazione TTN si vedono i dati dei sensori.

- In device "payload formatter" riportare:
```
function Decoder(bytes, port) {
 var result = ""; 
 for (var i = 0; i < bytes.length; i++) { 
   result += String.fromCharCode(parseInt(bytes[i])); 
 } 
 return { payload: result, };
}
```
- Nel menù a sinistra (applicazione) > Integrations > Storage integration > attivare questa integrazione

### 3. Home assistant
- Installare l'integrazione "The Things Network"
- Inserire i dati di autenticazione della propria applicazione... appare magicamente il device.
- A questo punto si ha a disposizione il sensore ad esempio:
```
template:
  - sensor:
      - name: "Garage"
        icon: mdi:car
        state: >-
          {% set values = states('sensor.eui_a8610a343je873hndj_payload').split(",") %}
          {% if values[0]|int <= 100 %}
            Occupato
          {% else %}
            Libero
          {% endif %}    
      - name: "Posto auto"
        icon: mdi:car
        state: >-
          {% set values = states('sensor.eui_a8610a343je873hndj_payload').split(",") %}
          {% if values[1]|int <= 300 %}
            Occupato
          {% else %}
            Libero
          {% endif %}  

```


Nota importante: allo stato attuale il componente standard non supporto le API v3. Installare quindi la versione disponibile in HACS che è più aggiornata.






