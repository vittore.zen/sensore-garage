/lora servers
add address=eu1.cloud.thethings.network down-port=1700 name=TTN-EU up-port=\
    1700
add address=us.mikrotik.thethings.industries down-port=1700 name=TTN-US \
    up-port=1700
add address=eu1.cloud.thethings.industries down-port=1700 name=\
    "TTS Cloud (eu1)" up-port=1700
add address=nam1.cloud.thethings.industries down-port=1700 name=\
    "TTS Cloud (nam1)" up-port=1700
add address=au1.cloud.thethings.industries down-port=1700 name=\
    "TTS Cloud (au1)" up-port=1700
add address=eu1.cloud.thethings.network down-port=1700 name="TTN V3 (eu1)" \
    up-port=1700
add address=nam1.cloud.thethings.network down-port=1700 name="TTN V3 (nam1)" \
    up-port=1700
add address=au1.cloud.thethings.network down-port=1700 name="TTN V3 (au1)" \
    up-port=1700
/ip dhcp-client
add interface=ether1
/ip firewall nat
add action=masquerade chain=srcnat out-interface=ether1
/lora
set 0 antenna=uFL antenna-gain=6dBi disabled=no servers=TTN-EU spoof-gps=\
    45.489433/12.206511/50m
/system clock
set time-zone-name=Europe/Rome
/system identity
set name=LoRaWAN
/system ntp client servers
add address=212.45.144.206
add address=135.125.165.132
