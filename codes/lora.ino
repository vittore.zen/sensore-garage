#include <MKRWAN.h>
#include <arduino-timer.h>

#define trigPin1 6
#define echoPin1 7
#define trigPin2 8
#define echoPin2 9

// Define variables:
volatile int stato = LOW;

String appEui = "0000000000000000";
String appKey = "xyz";

LoRaModem modem;

auto timer = timer_create_default(); 

void setup() {
  pinMode(trigPin1, OUTPUT);
  pinMode(echoPin1, INPUT);
  pinMode(trigPin2, OUTPUT);
  pinMode(echoPin2, INPUT);
  
//  Serial.begin(115200);
  while (!Serial);
  if (!modem.begin(EU868)) {
    Serial.println("Failed to start module");
    while (1) {}
  };
  Serial.print("Your module version is: ");
  Serial.println(modem.version());
  Serial.print("Your device EUI is: ");
  Serial.println(modem.deviceEUI());

  int connected = modem.joinOTAA(appEui, appKey);
  if (!connected) {
    Serial.println("Something went wrong; are you indoor? Move near a window and retry");
    while (1) {}
  }

  modem.minPollInterval(60);
  timer.every(15000, send_data);
 }

bool send_data(void *) {
  int distance1;
  long duration1;
  int distance2;
  long duration2;
  int err;
  String message;

  Serial.println("Read radar 1");
  digitalWrite(trigPin1, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin1, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin1, LOW);
  duration1 = pulseIn(echoPin1, HIGH);
  distance1 = duration1 * 0.034 / 2;

  delay(100);
  Serial.println("Read radar 2");
  digitalWrite(trigPin2, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin2, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin2, LOW);
  duration2 = pulseIn(echoPin2, HIGH);
  distance2 = duration2 * 0.034 / 2;

  delay(100);

  message=String(distance1)+","+String(distance2);

  Serial.print("Distance (cm) = ");
  Serial.println(message);
  modem.beginPacket();
  modem.print(message);
  err = modem.endPacket(true);
  if (err > 0) {
    Serial.println("Message sent correctly!");
  } else {
    Serial.println("Error sending message :(");
    Serial.println("(You may send a limited amount of messages per minute, depending on the signal strength");
    Serial.println("It may vary from 1 message every couple of seconds to 1 message every minute)");
  }
  return true; // keep timer active? true
}


void loop() {
   timer.tick(); // tick the timer 
}